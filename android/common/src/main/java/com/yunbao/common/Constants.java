package com.yunbao.common;

// +----------------------------------------------------------------------
// | Created by Yunbao
// +----------------------------------------------------------------------
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: https://gitee.com/yunbaokeji/
// +----------------------------------------------------------------------
// | Date: 2022-02-17
// +----------------------------------------------------------------------


public class Constants {
    public static final String URL = "url";
    public static final String PAYLOAD = "payload";
    public static final String SEX = "sex";
    public static final String NICK_NAME = "nickname";
    public static final String SIGN = "sign";
    public static final String TO_UID = "toUid";
    public static final String FROM_LIVE_ROOM = "fromLiveRoom";
    public static final String TO_NAME = "toName";
    public static final String STREAM = "stream";
    public static final String UID = "uid";
    public static final String TIP = "tip";
    public static final String CLASS_ID = "classID";
    public static final String CLASS_NAME = "className";
    public static final String CHECKED_ID = "checkedId";
    public static final String CHECKED_COIN = "checkedCoin";
    public static final String LIVE_DANMU_PRICE = "danmuPrice";
    public static final String COIN_NAME = "coinName";
    public static final String AT_NAME = "atName";
    public static final String LIVE_BEAN = "liveBean";
    public static final String LIVE_TYPE = "liveType";
    public static final String LIVE_KEY = "liveKey";
    public static final String LIVE_POSITION = "livePosition";
    public static final String LIVE_TYPE_VAL = "liveTypeVal";
    public static final String LIVE_UID = "liveUid";
    public static final String LIVE_STREAM = "liveStream";
    public static final String LIVE_HOME = "liveHome";
    public static final String LIVE_CLASS_PREFIX = "liveClass_";
    public static final String HAS_GAME = "hasGame";
    public static final String OPEN_FLASH = "openFlash";
    public static final String DIAMONDS = "钻石";
    public static final String VOTES = "映票";
    public static final String SCORE = "积分";
    public static final String PAY_TYPE_ALI = "ali";
    public static final String PAY_TYPE_WX = "wx";
    public static final String PAY_TYPE_PAYPAL = "paypal";
    public static final String PAY_BUY_COIN_ALI = "Charge.getAliOrder";
    public static final String PAY_BUY_COIN_WX = "Charge.getWxOrder";

    public static final String PACKAGE_NAME_ALI = "com.eg.android.AlipayGphone";//支付宝的包名
    public static final String PACKAGE_NAME_WX = "com.tencent.mm";//微信的包名
    public static final String COPY_PREFIX = "copy://";
    public static final String TEL_PREFIX = "tel://";
    public static final String AUTH_PREFIX = "auth://";
    public static final int GUARD_TYPE_NONE = 0;
    public static final int GUARD_TYPE_MONTH = 1;
    public static final int GUARD_TYPE_YEAR = 2;


    public static final int SETTING_MODIFY_PWD = 15;
    public static final int SETTING_UPDATE_ID = 16;
    public static final int SETTING_CLEAR_CACHE = 18;
    public static final int SEX_MALE = 1;
    public static final int SEX_FEMALE = 2;
    public static final int FOLLOW_FROM_SEARCH = 1004;
    //直播房间类型
    public static final int LIVE_TYPE_NORMAL = 0;//普通房间
    public static final int LIVE_TYPE_PWD = 1;//密码房间
    public static final int LIVE_TYPE_PAY = 2;//收费房间
    public static final int LIVE_TYPE_TIME = 3;//计时房间
    //主播直播间功能
    public static final int LIVE_FUNC_CAMERA = 2002;//切换摄像头
    public static final int LIVE_FUNC_FLASH = 2003;//闪光灯
    //socket
    public static final String SOCKET_CONN = "conn";
    public static final String SOCKET_BROADCAST = "broadcastingListen";
    public static final String SOCKET_SEND = "broadcast";
    public static final String SOCKET_STOP_PLAY = "stopplay";//超管关闭直播间
    public static final String SOCKET_STOP_LIVE = "stopLive";//超管关闭直播间
    public static final String SOCKET_SEND_MSG = "SendMsg";//发送文字消息，点亮，用户进房间  PS:这种混乱的设计是因为服务器端逻辑就是这样设计的,客户端无法自行修改
    public static final String SOCKET_LIGHT = "light";//飘心
    public static final String SOCKET_SEND_GIFT = "SendGift";//送礼物
    public static final String SOCKET_SEND_BARRAGE = "SendBarrage";//发弹幕
    public static final String SOCKET_LEAVE_ROOM = "disconnect";//用户离开房间
    public static final String SOCKET_LIVE_END = "StartEndLive";//主播关闭直播
    public static final String SOCKET_SYSTEM = "SystemNot";//系统消息
    public static final String SOCKET_KICK = "KickUser";//踢人
    public static final String SOCKET_SHUT_UP = "ShutUpUser";//禁言
    public static final String SOCKET_SET_ADMIN = "setAdmin";//设置或取消管理员
    public static final String SOCKET_CHANGE_LIVE = "changeLive";//切换计时收费类型
    public static final String SOCKET_UPDATE_VOTES = "updateVotes";//门票或计时收费时候更新主播的映票数
    public static final String SOCKET_FAKE_FANS = "requestFans";//僵尸粉

    public static final int SOCKET_WHAT_CONN = 0;
    public static final int SOCKET_WHAT_DISCONN = 2;
    public static final int SOCKET_WHAT_BROADCAST = 1;
    //socket 用户类型
    public static final int SOCKET_USER_TYPE_NORMAL = 30;//普通用户
    public static final int SOCKET_USER_TYPE_ADMIN = 40;//房间管理员
    public static final int SOCKET_USER_TYPE_ANCHOR = 50;//主播

    //提现账号类型，1表示支付宝，2表示微信，3表示银行卡
    public static final int CASH_ACCOUNT_ALI = 1;
    public static final int CASH_ACCOUNT_WX = 2;
    public static final int CASH_ACCOUNT_BANK = 3;
    public static final String CASH_ACCOUNT_ID = "cashAccountID";
    public static final String CASH_ACCOUNT = "cashAccount";
    public static final String CASH_ACCOUNT_NAME = "cashAccountName";
    public static final String CASH_ACCOUNT_TYPE = "cashAccountType";


    public static final String MOB_QQ = "qq";
    public static final String MOB_QZONE = "qzone";
    public static final String MOB_WX = "wx";
    public static final String MOB_WX_PYQ = "wchat";
    public static final String MOB_FACEBOOK = "facebook";
    public static final String MOB_TWITTER = "twitter";
    public static final String MOB_PHONE = "phone";

    public static final int LIVE_SDK_TX = 1;//腾讯推流

    public static final int GIFT_TYPE_NORMAL = 0;//正常礼物
    public static final String MALL_GOODS_ID = "mallGoodsId";
    public static final String NOT_LOGIN_UID = "-9999";//未登录的uid
    public static final String NOT_LOGIN_TOKEN = "-9999";//未登录的token
    public static final String EMPTY_STRING = "";


}
