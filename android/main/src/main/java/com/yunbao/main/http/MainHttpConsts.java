package com.yunbao.main.http;

// +----------------------------------------------------------------------
// | Created by Yunbao
// +----------------------------------------------------------------------
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: https://gitee.com/yunbaokeji/
// +----------------------------------------------------------------------
// | Date: 2022-02-17
// +----------------------------------------------------------------------
public class MainHttpConsts {
    public static final String LOGIN = "setLoginInfo";
    public static final String LOGIN_BY_THIRD = "loginByThird";
    public static final String GET_LOGIN_INFO = "getLoginInfo";
    public static final String REQUEST_BONUS = "requestBonus";
    public static final String GET_BONUS = "getBonus";
    public static final String SET_DISTRIBUT = "setDistribut";
    public static final String CHECK_AGENT = "checkAgent";
    public static final String GET_HOT = "getHot";
    public static final String GET_BASE_INFO = "getBaseInfo";
    public static final String SET_BLACK = "setBlack";
    public static final String GET_USER_HOME = "getUserHome";
    public static final String GET_SETTING_LIST = "getSettingList";
    public static final String SEARCH = "search";
    public static final String UPDATE_AVATAR = "updateAvatar";
    public static final String UPDATE_FIELDS = "updateFields";
    public static final String GET_PROFIT = "getProfit";
    public static final String GET_USER_ACCOUNT_LIST = "GetUserAccountList";
    public static final String ADD_CASH_ACCOUNT = "addCashAccount";
    public static final String DEL_CASH_ACCOUNT = "deleteCashAccount";
    public static final String DO_CASH = "doCash";
    public static final String GET_CLASS_LIVE = "getClassLive";
    public static final String GET_REGISTER_CODE = "getRegisterCode";
    public static final String REGISTER = "register";
    public static final String FIND_PWD = "findPwd";
    public static final String GET_FIND_PWD_CODE = "getFindPwdCode";
    public static final String MODIFY_PWD = "modifyPwd";
    public static final String SET_AUTH = "setAuth";



}
